# CRUD de Notícias

Este é um teste proposto na entrevista do dia 08/02/2021 e concluído em 15/02/2021.

O aplicativo é um CRUD de notícias, usando React/Redux e uma api Python com Mongo DB.

Os projetos são genéricos e independentes.

## Ferramentas necessárias:

* **Docker**, versão 19 ou superior;
* **Python**, versão 3.7 ou superior;
* **nodeJS**, v14 ou superior, com **NPM** v6.14 ou superior;
* **Terminal** do sistema operacional, dispostos em abas ou janelas, para executar os comandos.

## Download do código:

Abra uma janela de terminal para fazer download do repositório, que se encontra no bitbucket. Este repositório contém as duas aplicações:

- **app-react**
- **api-python**

`git clone git@bitbucket.org:dougcpp/crud-news.git`

 
## Banco de dados.
  
A solução escolhida foi o *MongoDB NoSQL*. Ele será instalado em um container *Docker*. Para isto, precisamos abrir uma nova aba ou janela de terminal e seguir os seguintes passos:

1. Faça o download de uma imagem MongoDB com comando:

    `docker pull mongo`
    
2. Após baixar a imagem, crie o container com a mesma, usando o comando com as configurações:

    `docker create -it --name Mongo -p 27017:27017 mongo`

3. Inicialize o container recém-criado:

    `docker start Mongo`

  
## Python:

Para codificar a api, a tecnologia escolhida foi *Python*. É preciso que esteja instalado o gerenciador de pacotes **pip** e a máquina de ambiente virtual, denominada **virtualenv**. Este ambiente instalado, irá empacotar todas as dependências que o projeto precisa, funcionando de modo independente.

Separe mais uma janela ou aba de terminal, pois precisaremos deixar servidores em execução, enquanto rodamos outros.

1. Para saber se a virtualenv está presente, basta executar o comando *virtualenv --version*. Caso não esteja instalada, execute o comando:

    `pip install virtualenv`

2. Tendo instalado, acesse através do terminal, o diretório raiz da api denominada *api-python*, que baixamos anteriormente do bitbucket. Nesta pasta raiz, crie um ambiente virtual, chamado "venv", com o comando:

    `virtualenv venv`

3. Após instalada, será preciso ativá-la com o comando:

    `source venv/bin/activate`

4. Instale os pacotes requeridos para a execução da api:

    `pip install -r requirements.txt`

5. Execute o python:

    `python app.py`

  
## React

1. Certifique-se que o **NodeJS** e o gerenciador de pacotes **NPM** estejam instalados. Use uma nova janela ou aba de terminal, mantendo os outros serviços em execução e acesse o diretório raiz da aplicação react *app-react*. Em seguida, digite:

    `npm install`

2. Aguarde a instalação completa e em seguida execute o projeto para teste, com o comando

    `npm start`

